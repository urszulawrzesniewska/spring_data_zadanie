package pl.cm;

import static java.lang.System.out;
import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.contains;
import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.ignoreCase;

import java.util.List;
import java.util.Optional;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.PageRequest;

import pl.cm.model.User;
import pl.cm.model.UserBuilder;
import pl.cm.repository.SimpleUserRepository;

public class App {

  public static void main(String[] args){
    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring-configuration.xml");
    final SimpleUserRepository userRepository = context.getBean(SimpleUserRepository.class);

    out.println("Find by username optional result");
    final Optional<User> optionalByUsername = userRepository.findByUsername("itzel.cassin");
    out.println(optionalByUsername);

    out.println("Find by username ");
    final User byUsername = userRepository.findByTheUsersName("itzel.cassin");
    out.println(byUsername);

    out.println("Find by lastName");
    final List<User> byLastname = userRepository.findByLastname("White");
    out.println(byLastname);

    out.println("Remove");
    final Long removedCount = userRepository.removeByLastname("MacGyver");
    out.println(removedCount);

    out.println("Pageable");
    out.println(userRepository.findByLastnameOrderByUsernameAsc("White", new PageRequest(0,2)));
    out.println(userRepository.findByLastnameOrderByUsernameAsc("White", new PageRequest(1,2)));
    out.println(userRepository.findByLastnameOrderByUsernameAsc("White", new PageRequest(2,2)));

    out.println("Find first 2 with order");
    final List<User> first2ByOrderByLastnameAsc = userRepository.findFirst2ByOrderByLastnameAsc();
    out.println(first2ByOrderByLastnameAsc);

    out.println("Find first 2 with order");
    final List<User> byFirstnameOrLastname = userRepository.findByFirstnameOrLastname("White");
    out.println(byFirstnameOrLastname);

    out.println("Find first 2 with order SpEl");
    final User user = UserBuilder.anUser().withFirstname("White").withLastname("White").build();
    final Iterable<User> byFirstnameOrLastnameSpEl = userRepository.findByFirstnameOrLastname(user);
    out.println(byFirstnameOrLastnameSpEl);


    out.println("Find By example firsname");
    final User userByExample = UserBuilder.anUser().withFirstname("White").build();
    final Iterable<User> byExampleFirstName = userRepository.findAll(Example.of(userByExample));
    out.println(byExampleFirstName);

    out.println("Find By example firsname and lastname");
    final User userByExampleAnd = UserBuilder.anUser().withFirstname("White").withLastname("White").build();
    final Iterable<User> byExampleAnd = userRepository.findAll(Example.of(userByExampleAnd));
    out.println(byExampleAnd);

    out.println("Find By example firsname or lastname");
    final User userByExampleOr = UserBuilder.anUser().withFirstname("White").withLastname("White").build();
    final Iterable<User> byExampleOr = userRepository.findAll(Example.of(userByExampleOr, ExampleMatcher.matchingAny()));
    out.println(byExampleOr);

    out.println("Find users above 50 years old");
    final List<User> findByAgeGreaterThan = userRepository.findByAgeGreaterThan(50);
    out.println(findByAgeGreaterThan);

    out.println("Find users below 20 years old");
    final List<User> findByAgeLessThan = userRepository.findByAgeLessThan(20);
    out.println(findByAgeLessThan);

    out.println("Find users between 20 and 50 years old");
    final List<User> findByAgeBetween = userRepository.findByAgeBetween(20,50);
    out.println(findByAgeBetween);

    out.println("Find users between 20 and 50 years old age ascending");
    final List<User> findByAgeBetweenOrderByAgeAsc = userRepository.findByAgeBetweenOrderByAgeAsc(20,50);
    out.println(findByAgeBetweenOrderByAgeAsc);

    out.println("Find users between 20 and 50 years old age ascending, first 5");
    final List<User> findFirst5ByAgeBetweenOrderByAgeAsc = userRepository.findFirst5ByAgeBetweenOrderByAgeAsc(20,50);
    out.println(findFirst5ByAgeBetweenOrderByAgeAsc);

    out.println("Pageable age");
    out.println(userRepository.findByAgeBetweenOrderByAgeAsc(40,50, new PageRequest(0,5)));
    out.println(userRepository.findByAgeBetweenOrderByAgeAsc(40,50, new PageRequest(1,5)));
    out.println(userRepository.findByAgeBetweenOrderByAgeAsc(40,50, new PageRequest(2,5)));

    out.println("find By Firstname Starting With Order By First name Desc");
    final List<User> findByFirstnameStartingWithOrderByFirstnameDesc = userRepository.findByFirstnameStartingWithOrderByFirstnameDesc("W");
    out.println(findByFirstnameStartingWithOrderByFirstnameDesc);

    out.println("find By First name Starting With Order By Last Name");
    final List<User> findByFirstnameStartingWithOrderByLastName = userRepository.findByFirstnameStartingWithOrderByLastName("An");
    out.println(findByFirstnameStartingWithOrderByLastName);

    out.println("find By Last name Ending With And Age Greater Than");
    final List<User> findByLastnameEndingWithAndAgeGreaterThan = userRepository.findByLastnameEndingWithAndAgeGreaterThan("te",40);
    out.println(findByLastnameEndingWithAndAgeGreaterThan);


    User daniel = new User();
    daniel.setFirstname("Crawford");

    ExampleMatcher matcher = ExampleMatcher.matching()
            .withMatcher("firstname",ignoreCase());
    Example<User> example = Example.of(daniel);
    final List<User> wyniki = (List<User>)userRepository.findAll(example);
    out.println(wyniki);

    User jacey = new User();
    jacey.setFirstname("Jacey");

    ExampleMatcher matcher1 = ExampleMatcher.matching()
            .withMatcher("firstname",ignoreCase()).withMatcher("lastname",contains());

    Example<User> example2 = Example.of(jacey);
    final List<User> wyniki1 = (List<User>)userRepository.findAll(example2);
    out.println(wyniki1);
  }
}
