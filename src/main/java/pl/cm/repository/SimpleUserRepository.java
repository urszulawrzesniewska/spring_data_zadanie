
package pl.cm.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.transaction.annotation.Transactional;

import pl.cm.model.User;

@Transactional
public interface SimpleUserRepository extends CrudRepository<User, Long> , QueryByExampleExecutor<User> {

	Optional<User> findByUsername(String username);

	User findByTheUsersName(String username);

	List<User> findByLastname(String lastname);

	Long removeByLastname(String lastname);

	Slice<User> findByLastnameOrderByUsernameAsc(String lastname, Pageable page);

	List<User> findFirst2ByOrderByLastnameAsc();


	@Query("select u from User u where u.firstname = :name or u.lastname = :name")
	List<User> findByFirstnameOrLastname(@Param("name") String name);

	@Query("select u from User u where u.firstname = :#{#user.firstname} or u.lastname = :#{#user.lastname}")
	Iterable<User> findByFirstnameOrLastname(@Param("user") User user);

	List<User> findByAgeGreaterThan(int age);

	List<User> findByAgeLessThan(int age);

	List<User> findByAgeBetween(int min, int max);

	List<User> findByAgeBetweenOrderByAgeAsc(int min, int max);

	List<User> findFirst5ByAgeBetweenOrderByAgeAsc(int min, int max);

	Slice<User> findByAgeBetweenOrderByAgeAsc(int min, int max, Pageable page);

	List<User> findByFirstnameStartingWithOrderByFirstnameDesc(String name);

	@Query("select u from User u where u.firstname like ?1% order by u.lastname asc")
	List<User> findByFirstnameStartingWithOrderByLastName(String name);

	@Query("select u from User u where u.lastname like %?1 and u.age > ?2")
	List<User> findByLastnameEndingWithAndAgeGreaterThan(String name, int age);

}
